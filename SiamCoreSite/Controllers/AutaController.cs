﻿using AutoMapper;
using CoreHelpers.Generics;
using Microsoft.AspNetCore.Mvc;
using SiamCoreRepository;
using SiamCoreRepository.Definitions;
using SiamCoreServices;
using SiamCoreServices.Models;
using SiamCoreSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiamCoreSite.Controllers
{
    [Route("api/[controller]")]
    public class AutaController : Controller
    {

        private readonly IUsersService _usersService;

        private readonly ICoreSession _core;

        private readonly IAutaService _auta;

        public AutaController(IUsersService usersService, ICoreSession core, IAutaService auta) : base()
        {
            _usersService = usersService;
            _core = core;
            _auta = auta;
        }

        [HttpGet("List")]
        public GenericResponse<List<AutaTypyModel>> ListAuta()
        {
            var retVal = new GenericResponse<List<AutaTypyModel>>(null);
            try
            {
                var autaTypy = _auta.ListAutaTypy();
                
                retVal.data = new List<AutaTypyModel>();
                foreach(var auto in autaTypy)
                {
                    retVal.data.Add(Mapper.Map<AutaTypyDTO, AutaTypyModel>(auto));
                }

                retVal.isOk = true;
            }
            catch (Exception ex)
            {
                retVal.isOk = false;
                retVal.message = ex.Message;
            }

            return retVal;
        }

    }
}
