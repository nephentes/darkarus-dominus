﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiamCoreRepository.Definitions
{
    public class AutaTypyModel
    {

        public string marka { get; set; }

        public string importer { get; set; }

        public string model { get; set; }

        public string modelGrupa { get; set; }

        public string modelGrupa2 { get; set; }

        public string typNatCode { get; set; }

        public string typ { get; set; }

        public string segmentKrajowy { get; set; }

        public string segmentMiedzynarodowy { get; set; }

        public string segment { get; set; }

        public double mocKW { get; set; }

        public double mocKM { get; set; }

        public string paliwo { get; set; }

        public string nadwozie { get; set; }

        public int? iloscDrzwi { get; set; }

        public int? iloscCylindrow { get; set; }

        public string ukladCylindrow { get; set; }

        public double pojemnosc { get; set; }

        public double momentObr { get; set; }

        public string rodzajDoladowania { get; set; }

        public int? iloscZaworow { get; set; }

        public string rodzajKat { get; set; }

        public string normaEuro { get; set; }

        public string rodzajSkrzyni { get; set; }

        public string ukladNapedowy { get; set; }

        public int? iloscBiegowPrzod { get; set; }

        public int? rozstawOsi { get; set; }

        public int? DMC { get; set; }

        public int? iloscMiejsc { get; set; }

        public int? iloscMiejscMax { get; set; }

        public int? obciazenieDachuMax { get; set; }

        public int? dlugoscNad { get; set; }

        public int? dlugoscNadMax { get; set; }

        public int? szerokosc { get; set; }

        public int? szerokoscMax { get; set; }

        public int? wysokosc { get; set; }

        public int? wysokoscMax { get; set; }

        public int? pojemnoscBagMax { get; set; }

        public int? pojemnoscBag { get; set; }

        public int? pojemnoscBagMin { get; set; }

        public int? masaWlasna { get; set; }

    }

}
