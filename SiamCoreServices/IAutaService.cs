﻿using CoreHelpers.Templates;
using SiamCoreRepository.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiamCoreServices
{

    public interface IAutaService
    {

        List<AutaTypyDTO> ListAutaTypy();

    }

}
