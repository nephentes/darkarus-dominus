﻿using CoreHelpers;
using CoreHelpers.Templates;
using SiamCoreRepository;
using SiamCoreRepository.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiamCoreServices
{

    public class AutaService : IAutaService
    {

        private readonly IBaseDAL _baseDAL;

        private readonly ICoreSession _core;

        public AutaService(IBaseDAL baseDAL, ICoreSession core)
        {
            _baseDAL = baseDAL;
            _core = core;
        }

        public List<AutaTypyDTO> ListAutaTypy()
        {
            var retVal = new List<AutaTypyDTO>();
            retVal = _baseDAL.SelfContext.AutaTypy.Where(a => a.IloscCylindrow == 3).ToList();
            return retVal;
        }
    
    }
   
}
