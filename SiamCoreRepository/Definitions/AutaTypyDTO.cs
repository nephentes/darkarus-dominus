﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiamCoreRepository.Definitions
{
    [Table("auta_typy")]
    public class AutaTypyDTO
    {
        /*
        [Column("id", Order = 0)]
        public int Id { get; set; }
        */

        [Column("marka", Order = 1)]
        public string Marka { get; set; }

        [Column("importer", Order = 2)]
        public string Importer { get; set; }

        [Column("model", Order = 3)]
        public string Model { get; set; }

        [Column("model_grupa", Order = 4)]
        public string ModelGrupa { get; set; }

        [Column("model_grupa_2", Order = 5)]
        public string ModelGrupa2 { get; set; }

        [Column("typnatcode", Order = 6)]
        [Key]
        public string TypNatCode { get; set; }

        [Column("typ", Order = 7)]
        public string Typ { get; set; }

        [Column("segment_krajowy", Order = 8)]
        public string SegmentKrajowy { get; set; }

        [Column("segment_miedzynarodowy", Order = 9)]
        public string SegmentMiedzynarodowy { get; set; }

        [Column("segment", Order = 10)]
        public string Segment { get; set; }

        [Column("moc_kw", Order = 11)]
        public double MocKW { get; set; }

        [Column("moc_km", Order = 12)]
        public double MocKM { get; set; }

        [Column("paliwo", Order = 13)]
        public string Paliwo { get; set; }

        [Column("nadwozie", Order = 14)]
        public string Nadwozie { get; set; }

        [Column("ilosc_drzwi", Order = 15)]
        public int? IloscDrzwi { get; set; }

        [Column("ilosc_cylindrow", Order = 16)]
        public int? IloscCylindrow { get; set; }

        [Column("uklad_cylindrow", Order = 17)]
        public string UkladCylindrow { get; set; }

        [Column("pojemnosc", Order = 18)]
        public double Pojemnosc { get; set; }

        [Column("moment_obrotowy", Order = 19)]
        public double MomentObr { get; set; }

        [Column("rodzaj_doladowania", Order = 20)]
        public string RodzajDoladowania { get; set; }

        [Column("ilosc_zaworow_na_cylinder", Order = 21)]
        public int? IloscZaworow { get; set; }

        [Column("rodzaj_katalizatora", Order = 22)]
        public string RodzajKat { get; set; }

        [Column("norma_euro", Order = 23)]
        public string NormaEuro { get; set; }

        [Column("rodzaj_skrzyni_biegow", Order = 24)]
        public string RodzajSkrzyni { get; set; }

        [Column("uklad_napedowy", Order = 25)]
        public string UkladNapedowy { get; set; }

        [Column("ilosc_biegow_do_przodu", Order = 26)]
        public int? IloscBiegowPrzod { get; set; }

        [Column("rozstaw_osi", Order = 27)]
        public int? RozstawOsi { get; set; }

        [Column("dopuszczalna_masa_calokowita", Order = 28)]
        public int? DMC { get; set; }

        [Column("ilosc_miejsc", Order = 29)]
        public int? IloscMiejsc { get; set; }

        [Column("max_ilosc_miejsc", Order = 30)]
        public int? IloscMiejscMax { get; set; }

        [Column("max_obciazenie_dachu", Order = 31)]
        public int? ObciazenieDachuMax { get; set; }

        [Column("dlugosc_nadwozia", Order = 32)]
        public int? DlugoscNad { get; set; }

        [Column("max_dlugosc_nadwozia", Order = 33)]
        public int? DlugoscNadMax { get; set; }

        [Column("szerokosc", Order = 34)]
        public int? Szerokosc { get; set; }

        [Column("max_szerokosc", Order = 35)]
        public int? SzerokoscMax { get; set; }

        [Column("wysokosc", Order = 36)]
        public int? Wysokosc { get; set; }

        [Column("max_wysokosc", Order = 37)]
        public int? WysokoscMax { get; set; }

        [Column("max_pojemnosc_bagaznika", Order = 38)]
        public int? PojemnoscBagMax { get; set; }

        [Column("pojemnosc_bagaznika", Order = 39)]
        public int? PojemnoscBag { get; set; }

        [Column("min_pojemnosc_bagaznika", Order = 40)]
        public int? PojemnoscBagMin { get; set; }

        [Column("masa_wlasna", Order = 41)]
        public int? MasaWlasna { get; set; }

    }

}
